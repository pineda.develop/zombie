# Set base image (host OS)
FROM python:3.9.5-slim-buster

# By default, listen on port 5000
EXPOSE 80

# Copy the dependencies file to the working directory
COPY requirements.txt .

# ---------------FOR PACKAGE psycopg2----------------
RUN apt-get update \
    && apt-get -y install libpq-dev gcc \
    && pip install psycopg2
RUN pip install fastapi uvicorn  
RUN pip install requests 
# Install any dependencies
RUN pip install -r requirements.txt

# Copy the content of the local src directory to the working directory
COPY ./app /app

# Specify the command to run on container start
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
