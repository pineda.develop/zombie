from app.brains.models import SoloUser, SoloUserLoginCredentials
import random 
import string

def create_user(db):
    """Create a new user 
    Args:
        db (_type_): a database connection 

    Returns:
        new_added_user: row info 
    """
    try:
        # remove any white space from the email 
        random_generated_user_name = ''.join(random.choices(string.ascii_letters + string.digits, k=12))

        # Create the new user row 
        solo_user_entry = SoloUser({
            'user_id':random_generated_user_name ,
        })

        # Add the new user to the database 
        db.add(solo_user_entry)
        db.commit()
        db.refresh(solo_user_entry)

        # Get the new added user from the database and assign their id number to variable new_added_user_id
        user = get_user_by_id(db, random_generated_user_name)

        return user 
    except Exception as e:
        db.rollback()
        raise(e)


def get_user_by_id(db, user_id: str) -> SoloUser:
    """Query one user from the database by mathing id 

    Args:
        db (session): a database connection 
        user_id (str): the unique id of the user 

    Returns:
        SoloUser:  an object with the users account information 
    """
    user_entry = db.query(SoloUser).filter_by(user_id=user_id).first()
    return user_entry


def get_user_by_email(db, user_id: str) -> SoloUser:
    """Query the database for one user that matches by email 

    Args:
        db (session): a database connection 
        email (str): email of user 

    Returns:
        SoloUser: an object with the users account information 
    """
    user_entry = db.query(SoloUser).filter_by(user_id=user_id).first()
    return user_entry



def get_user_login_credentials(db, email:str) -> SoloUserLoginCredentials:
    """Query the database for a user login credentials by email 

    Args:
        db ([type]): a database connection 
        email (str): email of user 

    Returns:
        SoloUserLoginCredentials: an object with the users login information 
    """
    user_login_info = db.query(SoloUserLoginCredentials).filter_by(email=email).first()
    return user_login_info


