from fastapi import FastAPI, HTTPException, Depends, Request, APIRouter
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import JWTDecodeError, AuthJWTException
from pydantic import BaseModel
from app.helpers import user_helpers
from app.config import CONFIGURATION
from functools import wraps
import faulthandler
from app.brains.database import SessionLocal

router = APIRouter()

class Settings(BaseModel):
    authjwt_secret_key: str = CONFIGURATION['FAST_API_SETTINGS']['ZOMBIE_API_KEY']

@AuthJWT.load_config
def get_config():
    return Settings()


def app_access_required(fn):
    """Checks if a user has access to the application

    Args:
        fn (function): a function 

    Raises:
        HTTPException (status_code = 401): Signature has expired
        HTTPException (status_code = 403): No web access

    Returns:
        wrapper: a wrapper for the function 
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        faulthandler.enable()
   
        try:
            Authorize = kwargs.get('Authorize')
            Authorize.jwt_required()
            context_session = kwargs.get('context_session')
            user_id = Authorize.get_jwt_subject()
            if context_session is not None:
                    db = SessionLocal()
                    user_info = user_helpers.get_user_by_id(db, user_id)
                    db.close()
            else:
                user_info = None
        except JWTDecodeError as e:
            if e.message == 'Signature has expired':
                print("e-Message", e.message)
                return {"status_code": 401,"message":"Signature has expired" } 
        app_access = Authorize.get_raw_jwt().get('app')
        if not app_access:
            return {"status_code": 403,"message":"No app access"} 
        else:
            return fn(*args, **kwargs)
    return wrapper


def create_oauth_state_token(state: dict, Authorize = None):
    """Convert your state into an a sign token with AuthJWT

    Args:
        state (dict): the state for the api request 
        Authorize (AuthJWT, optional):  JWT token with signed information. Defaults to None.

    Returns:
        state_token (str): the state converted into a signed access token 
    """
    if Authorize is None:
        Authorize = AuthJWT()
    state_token = Authorize.create_access_token(subject=1, user_claims=state)
    return state_token



def create_access_token(user_id: str, Authorize = None):
    """Generate a token for app access 

    Args:
        user_id (str): the id of the user to grant access to 
        Authorize (AuthJWT, optional):  JWT token with signed information . Defaults to None.

    Returns:
        app_access_token (str): a token to grant access to the application 
    """

    if Authorize is None:
        Authorize = AuthJWT()
    app_access_token = Authorize.create_access_token(subject=user_id, user_claims={'app': True},
                                                     expires_time=3*60*60)
    return app_access_token


def create_refresh_token(user_id: str, Authorize = None):
    """Generate a refresh token for a user

    Args:
        user_id (str): the id of the user 
        Authorize (AuthJWT, optional): JWT token with signed information . Defaults to None.

    Returns:
        refresh_token: a new refresh token 
    """

    if Authorize is None:
        Authorize = AuthJWT()
    refresh_token = Authorize.create_refresh_token(subject=user_id)
    return refresh_token



def generate_tokens(user_id, Authorize=None):
    """Generate an access and refresh token 

    Args:
        user_model (SoloUser): an object of the user account information 
        Authorize (AuthJWT, optional): JWT token with signed information . Defaults to None.
      
    Returns:
        (dict): a dictionary with the new access_token and refresh_token 
    """

    if Authorize is None:
        Authorize = AuthJWT()

    access_token = create_access_token(user_id, Authorize)
    refresh_token = create_refresh_token(user_id, Authorize)
    return {
        'access_token': access_token,
        'refresh_token': refresh_token
    }


