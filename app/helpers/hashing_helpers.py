from argon2 import PasswordHasher
import random 
import string 


ph = PasswordHasher()

def hash_password(password):
    random_number = random.randrange(1,100)
    random_string = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(random_number))
    combined_string = random_string + str(password)
    password_hashed = ph.hash(combined_string.encode("utf-8"))
    return password_hashed, random_string


def verify_hash_password(test_password, correct_hashed_password):
    try: 
        answer = ph.verify(correct_hashed_password, test_password)
    except:
        answer = False 
    return answer 