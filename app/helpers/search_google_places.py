import requests
from app.config import CONFIGURATION


class GooglePlacesAPIHelpers():
    def query_auto_suggestion(self, user_input: str):
        """
        From Google Maps API: 
        The Place Autocomplete service is a web service that returns place predictions in response to an HTTP request. The request specifies a textual search string and optional geographic bounds. 
        The service can be used to provide autocomplete functionality for text-based geographic searches, by returning places such as businesses, addresses and points of interest as a user types.
        
        Required Params:
            input : str "what to search for"

        
        Returns:
            - An array of json objects of predictions within a radius 
                  
        """

        # Set up requirements for HTTP request 
        api_key = CONFIGURATION['GOOGLE_PLACES']['API_KEY']
        url = f"https://maps.googleapis.com/maps/api/place/autocomplete/json?input={user_input}&types=establishment&components=country:us&key={api_key}"
        payload={}
        headers = {}

        # Make the request
        response = requests.get( url, headers=headers, data=payload)
        response_json = response.json()
        predictions_array = response_json.get('predictions')

        # If there is no predictions in the array then return an empty array
        if response_json.get('error_message'):
            raise Exception(response_json['error_message'])
        elif not predictions_array:
            predictions_array = []


        return {"result":predictions_array}

