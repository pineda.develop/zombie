from app.brains.models import SpotifyClientCredentials
from app.brains.models import SpotifyAccounts
from app.config import CONFIGURATION
import json 
import requests 
import base64



def add_spotify_account(db, name: str, credentials_unencrypted: dict, user_id: str):
    print('new accountboy')
    
    # encrypt credentials
    credentials_unencrypted_json = json.dumps(credentials_unencrypted).encode('ascii')
    # credentials_encrypted = kms_helper.encrypt_text(credentials_unencrypted_json)

    spotify_account = SpotifyAccounts({
        'name': name,
        'created_by_user_id': user_id,
        'credentials': credentials_unencrypted_json,
    })
    db.add(spotify_account)
    db.commit()
    db.refresh(spotify_account)
    

    return "Done"

def check_for_spotify_account(db, user_id: str):
    """Checks to see if the user has a Spotify account in our database 

    Args:
        db (Session): a database connection 
        user_id (str): the id of the user 

    Returns:
        bool: True if has an account, False if they do not have an account 
    """
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    if user_spotify_account:
        return True 
    
    return False 

def update_spotify_account_credentials(db, user_id: str, credentials):
    """Update Spotify credentials 

    Args:
        db (Session): a database connection
        user_id (str): the id of the user 
        credentials (dict): unencrypted dictionary of spotify credentials 
    """

    # encrypt credentials
    credentials_unencrypted_json = json.dumps(credentials).encode('ascii')
    # credentials_encrypted = kms_helper.encrypt_text(credentials_unencrypted_json)


    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    user_spotify_account.credentials = credentials_unencrypted_json
    db.commit()
    db.refresh(user_spotify_account)
    return True 


def get_refresh_token(db, user_id):
    
    # Get the user account 
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    
    # Get the Spotify refresh token from the users credentials 
    credentials_unencrypted_json = user_spotify_account.credentials
    # 
    credentials = json.loads(credentials_unencrypted_json)
    refresh_token = credentials['refresh_token']

    # Get the client id and secret for our Spotify Third Party App 
    client_id = CONFIGURATION['SPOTIFY']['CLIENT_ID']
    client_secret = CONFIGURATION['SPOTIFY']['CLIENT_SECRET']


    # Create the POST request 
    formatted_client = f'{client_id}:{client_secret}'
    auth_encrypt = base64.b64encode(formatted_client.encode()).decode()
    auth_string = f"Basic {auth_encrypt}"

    headers = {
        "Authorization": auth_string,
    }

    params = {
        "grant_type":"refresh_token",
        "refresh_token":refresh_token,
    }
    token_url = "https://accounts.spotify.com/api/token"

    r = requests.post(token_url, data=params, headers=headers)

    # If the call is successfull update the users spotify account with the new access token
    if r.status_code == 200: 
        # Get the new access token from the call 
        response = r.json()
        access_token = response['access_token']
        
        # Update the users old access token with the new token 
        credentials['access_token'] = access_token
        updated_account = update_spotify_account_credentials(db, user_id, credentials)
        if updated_account:
            return True 


    return False 
    
def delete_spotify_account(db, user_id:str):
    """Remove a Spotify account from our database. 
       -> We are unable to remove third party apps from Spotify through the API, 
        so the best we can do is remove the tokens and let the user know to remove the app directly. Maybe take them there 
        on the app and do it manually 

    Args:
        db (session): a database connection
        user_id (str): the id of the user 

    Returns:
        bool: True if deleted, False if not deleted 
    """
    deleted_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).delete()
    if not deleted_spotify_account:
        return False 
    else: 
        db.commit()
        return True


def play_song(db, user_id, track_id):
    
    # Get the user account 
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    
    # Decrypt and turn the credentials into a python object 
    credentials_unencrypted_json = user_spotify_account.credentials
    # 
    credentials = json.loads(credentials_unencrypted_json)
    
    # Set up API call 
    # track_id = "spotify:track:2QDuVo8Fei2fuqxjHNkUY3"
    headers = {
       'Authorization': 'Bearer {token}'.format(token=credentials['access_token'])
    }

    # Pass in tracklist 
    payload = {  "uris": [track_id], "position_ms": 0 }
    url = "https://api.spotify.com/v1/me/player/play"
    print('payload',payload)
    print("url",url)
    response = requests.put(url, headers=headers, json=payload)
    
    # Check if the access token expired 
    if response.status_code == 401:
        generated_new_tokens = get_refresh_token(db, user_id)
        if generated_new_tokens:
           return play_song(db, user_id, track_id)
        else:
            print("trouble generating new refresh tokens")

    if response.status_code == 204:
        return "PLAYING_SONG" 
    else: 
        return "ERROR"


def get_new_client_access_token(db):
    # Get the client id and secret for our Spotify Third Party App 
    client_id = CONFIGURATION['SPOTIFY']['CLIENT_ID']
    client_secret = CONFIGURATION['SPOTIFY']['CLIENT_SECRET']


    # Create the POST request 
    formatted_client = f'{client_id}:{client_secret}'
    auth_encrypt = base64.b64encode(formatted_client.encode()).decode()
    auth_string = f"Basic {auth_encrypt}"

    headers = {
        "Authorization": auth_string,
    }
    params = {
        "grant_type":"client_credentials",
        "content_type": "application/x-www-form-urlencoded",
    }
    token_url = "https://accounts.spotify.com/api/token"

    
    r = requests.post(token_url, data=params, headers=headers)

    # If the call is successfull update the users spotify account with the new access token
    if r.status_code == 200: 
        # Get the new access token from the call 
        response = r.json()
       
        access_token = response['access_token']
        

        # Check to see if we already have credentials saved in the database 
        client_spotify_credentials = db.query(SpotifyClientCredentials).first()
        if client_spotify_credentials:
            client_spotify_credentials.access_token = access_token
        else: 
            spotify_client_credentials = SpotifyClientCredentials({
                'access_token':access_token,
            })
            db.add(spotify_client_credentials)
        
        db.commit()
        db.refresh()
        
        return access_token

        



def get_playlist(db, next_url=None):
    client_credentials = db.query(SpotifyClientCredentials).first()
  

    print("Access Token",  client_credentials.access_token)
    playlist_id = "37i9dQZF1DXd9rSDyQguIk"

    headers = {
       'Authorization': 'Bearer {token}'.format(token= client_credentials.access_token)
    }

    if next_url == None:
        url = f"https://api.spotify.com/v1/playlists/{playlist_id}"
        response = requests.get(url, headers=headers)
    else: 
        response = requests.get(next_url, headers=headers)


    print("RESPonse", response.text)
    # Check if the access token expired 
    if response.status_code == 401:
        new_access_token = get_new_client_access_token(db)
        get_playlist(db, new_access_token, playlist_id, next_url)
      
    json_response = response.json()
    return json_response['tracks']['items']
    

def get_all_playlists(db, user_id):
    # Get the user account 
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    
    # Decrypt and turn the credentials into a python object 
    credentials_unencrypted_json = user_spotify_account.credentials
    credentials = json.loads(credentials_unencrypted_json)

    url = "https://api.spotify.com/v1/me/playlists"
    headers = {
       'Authorization': 'Bearer {token}'.format(token=credentials['access_token'])
    }
    response = requests.get(url, headers=headers)
    
    # Check if the access token expired 
    if response.status_code == 401:
        generated_new_tokens = get_refresh_token(db, user_id)
        if generated_new_tokens:
           return get_all_playlists(db, user_id)
        else:
            print("trouble generating new refresh tokens")

    print(response.json())
    return "YES"



def pause_player(db, user_id):
    
    # Get the user account 
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    
    # Decrypt and turn the credentials into a python object 
    credentials_unencrypted_json = user_spotify_account.credentials
    
    credentials = json.loads(credentials_unencrypted_json)
    
    url = "https://api.spotify.com/v1/me/player/pause"
    headers = {
       'Authorization': 'Bearer {token}'.format(token=credentials['access_token'])
    }
    response = requests.put(url, headers=headers)
    
    # Check if the access token expired 
    if response.status_code == 401:
        generated_new_tokens = get_refresh_token(db, user_id)
        if generated_new_tokens:
           return pause_player(db, user_id)
        else:
            print("trouble generating new refresh tokens")

    if response.status_code == 204:
        return "PLAYER_PAUSED"
    elif response.status_code == 403:
        return "NO_SONG_PLAYING"
    else: 
        return "ERROR"




def update_track_favorite_status(db, user_id, update_type, track_id):


    # Get the user account 
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    
    # Decrypt and turn the credentials into a python object 
    credentials_unencrypted_json = user_spotify_account.credentials
    
    credentials = json.loads(credentials_unencrypted_json)
    
    # track_id = "4iV5W9uYEdYUVa79Axb7Rh"
    url = "https://api.spotify.com/v1/me/tracks?ids="+track_id
   
    headers = {
       'Authorization': 'Bearer {token}'.format(token=credentials['access_token'])
    }

    print("URL", url)
    if update_type == "favorite":
        response = requests.put(url, headers=headers)
    else: 
        print('in here')
        response = requests.delete(url, headers=headers)
        print(response.status_code)

    # Check if the access token expired 
    if response.status_code == 401:
        generated_new_tokens = get_refresh_token(db, user_id)
        if generated_new_tokens:
           return update_track_favorite_status(db, user_id, update_type, track_id)
        else:
            print("trouble generating new refresh tokens")

    if response.status_code == 202 or response.status_code == 200:
        return "TRACK_UPDATED"
    else: 
        return "ERROR"



def get_current_user_profile(db, user_id):
    # Get the user account 
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    
    # Decrypt and turn the credentials into a python object 
    credentials_unencrypted_json = user_spotify_account.credentials
    
    credentials = json.loads(credentials_unencrypted_json)
    
    headers = {
       'Authorization': 'Bearer {token}'.format(token=credentials['access_token'])
    }

    url = "https://api.spotify.com/v1/me"
    response = requests.get(url, headers=headers)
    
    # Check if the access token expired 
    if response.status_code == 401:
        generated_new_tokens = get_refresh_token(db, user_id)
        if generated_new_tokens:
           return get_current_user_profile(db, user_id)
        else:
            print("trouble generating new refresh tokens")

    if response.status_code == 200: 
        profile = response.json()
        return profile






def get_saved_tracks(db, user_id, track_ids):

        # Get the user account 
    user_spotify_account = db.query(SpotifyAccounts).filter_by(created_by_user_id=user_id).first()
    
    # Decrypt and turn the credentials into a python object 
    credentials_unencrypted_json = user_spotify_account.credentials
    
    credentials = json.loads(credentials_unencrypted_json)
    
    headers = {
       'Authorization': 'Bearer {token}'.format(token=credentials['access_token'])
    }

    
    url = "https://api.spotify.com/v1/me/tracks/contains"
    # Current max of 50 tracks per request as of 2/15/2021
    params = {
        'ids': track_ids
    }
    response = requests.get(url, headers=headers, params=params)
    print("RESPONSE", response.content)
    # Check if the access token expired 
    if response.status_code == 401:
        generated_new_tokens = get_refresh_token(db, user_id)
        if generated_new_tokens:
           return get_saved_tracks(db, user_id)
        else:
            print("trouble generating new refresh tokens")

    if response.status_code == 200: 
        return response.json()
