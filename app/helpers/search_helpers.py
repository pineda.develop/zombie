from sqlalchemy.sql import text
import pandas as pd 

def time_convert(sec):
  mins = sec // 60
  sec = sec % 60
  hours = mins // 60
  mins = mins % 60
  print("Time Lapsed = {0}:{1}:{2}".format(int(hours),int(mins),sec))



def search_by_geo_location(db, long, lat):
    print("started")
    # Create the query to search by geo location 
    # stmt = text("SELECT *, point(:input_longitude, :input_latitude) <@> point(longitude, latitude)::point as distance FROM public.establishments WHERE (point(:input_longitude, :input_latitude) <@> point(longitude, latitude)) < :input_distance ORDER BY distance;")
    stmt = text("select * from public.establishments WHERE ST_DWithin(establishments.geography, ST_MakePoint(:input_longitude, :input_latitude)::geography, 50000);")

    # Allow only the two int parameters 
    stmt = stmt.bindparams(input_longitude=long, input_latitude=lat,)

    result = db.execute(stmt)
    df = pd.DataFrame(result.fetchall())
    df.columns = result.keys()
    dict_df = df.to_dict('records')

    print('ended')

    return dict_df
 

def search_by_text_input(db, text_input:str):

    stmt = text("select * from public.establishments where :user_input % any(string_to_array(name,' ')) or :user_input % any(string_to_array(address,' ')) limit 20;")

    # Allow only the two int parameters 
    stmt = stmt.bindparams(user_input=text_input)
    try:
      result = db.execute(stmt)
      df = pd.DataFrame(result.fetchall())
      df.columns = result.keys()
      dict_df = df.to_dict('records')
      return dict_df
    except: 
      return None 

