import aws_encryption_sdk
from aws_encryption_sdk import CommitmentPolicy
from app import config
import boto3
import botocore.session

botocore_session = botocore.session.get_session()


solo_key_id = config.CONFIGURATION['KMS']['SOLO_KEY_ID']
access_key_id = config.CONFIGURATION['AWS']['ACCESS_KEY']
aws_secret_access_key = config.CONFIGURATION['AWS']['SECRET_ACCESS_KEY']
boto3.setup_default_session(aws_access_key_id=access_key_id, aws_secret_access_key=aws_secret_access_key, region_name="us-east-1", botocore_session=botocore_session)

def encrypt_text(plaintext):
    plaintext_binary = plaintext.encode()

    client = aws_encryption_sdk.EncryptionSDKClient(commitment_policy=CommitmentPolicy.REQUIRE_ENCRYPT_REQUIRE_DECRYPT)
    kms_kwargs = dict(key_ids=[solo_key_id])
    master_key_provider = aws_encryption_sdk.StrictAwsKmsMasterKeyProvider(**kms_kwargs)
    cipher_text, _ = client.encrypt(source=plaintext_binary, key_provider=master_key_provider)
    return cipher_text

def decrypt_encrypted_binary(encrypted_binary):
    client = aws_encryption_sdk.EncryptionSDKClient(commitment_policy=CommitmentPolicy.REQUIRE_ENCRYPT_REQUIRE_DECRYPT)
    kms_kwargs = dict(key_ids=[solo_key_id])
    master_key_provider = aws_encryption_sdk.StrictAwsKmsMasterKeyProvider(**kms_kwargs)

    decrypted_binary, _ = client.decrypt(source=encrypted_binary, key_provider=master_key_provider)
    return decrypted_binary.decode()


