from fastapi_jwt_auth.auth_jwt import AuthJWT
from pyngrok.ngrok import update
from app.brains.models import SpotifyAccounts
from app.helpers.kms_helper import decrypt_encrypted_binary
from app.config import CONFIGURATION
from app.helpers import jwt_helpers, kms_helper, spotify_helpers, user_helpers
import json 
import requests
import base64


def generate_spotify_url(user_email:str , Authorize):
    """Generate the Spotify Authorization url 

    Args:
        user_id ([type]): [description]
        Authorize ([type]): [description]

    Returns:
        [type]: [description]
    """
    

    # Set up the basic requirements for the url 
    domain = CONFIGURATION["FAST_API_SETTINGS"]['DOMAIN']
    scopes = CONFIGURATION['SPOTIFY']['SCOPES']
    client_id = CONFIGURATION['SPOTIFY']['CLIENT_ID']
    scopes_string = '+'.join(scopes)
    redirect_url = f'https://{domain}/oauth/spotify-callback'


    # Create the state for improved security 
    state = {'user_email': user_email}
    state_jwt = jwt_helpers.create_oauth_state_token(state, Authorize)
    
    oauth_url = "https://accounts.spotify.com/authorize"
    url = f'{oauth_url}?client_id={client_id}&redirect_uri={redirect_url}&response_type=code&state={state_jwt}&scope' \
                  f'={scopes_string}'
    return url


def receive_spotify_oauth_code(db, state, code: None):
    """Receive the spotify oAuth code and request for a refresh token

    Args:
        db (Session): a database connection 
        state (JWT Token): an encrypted token of the state 
        code (str): the code returned from the Spotify API callback 
        user_email (str): the users email 

    Returns:
        (dict): {"message":"successfully added account"}
    """
    
    Authorize = AuthJWT()
    decoded_state = Authorize.get_raw_jwt(state)
    user_email = decoded_state.get('user_email')
    user_info_dict = user_helpers.get_user_by_email(db, user_email)
    
    # Set up requirements for refresh token for Spotify 
    token_url = "https://accounts.spotify.com/api/token"
    client_id = CONFIGURATION['SPOTIFY']['CLIENT_ID']
    client_secret = CONFIGURATION['SPOTIFY']['CLIENT_SECRET']
    domain = CONFIGURATION['FAST_API_SETTINGS']['DOMAIN']
    redirect_uri = f'https://{domain}/oauth/spotify-callback'

    formatted_client = f'{client_id}:{client_secret}'
    auth_encrypt = base64.b64encode(formatted_client.encode()).decode()
    auth_string = f"Basic {auth_encrypt}"

    params = {
        "grant_type":"authorization_code",
        "code":code,
        "redirect_uri":redirect_uri,
    }

    headers = {
        "Authorization": auth_string,
    }

    r = requests.post(token_url, data=params, headers=headers)
    response = json.loads(r.content)
    if user_info_dict:

        # Check if the user already has a Spotify account in our database
        # If not then create a new one 
        updateAnExistingAccount = spotify_helpers.check_for_spotify_account(db, user_info_dict.id)
        if updateAnExistingAccount:
            spotify_helpers.update_spotify_account_credentials(db, user_info_dict.id, response)
        else: 
            spotify_helpers.add_spotify_account(db, user_info_dict.name, response, user_info_dict.id  )

    return {"message":"successfully added account"}


