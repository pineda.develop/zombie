from fastapi import APIRouter
from app.routers import search, spotify, user

router = APIRouter()

router.include_router(search.router, prefix = '/search')
router.include_router(user.router, prefix = '/user')
router.include_router(spotify.router, prefix = '/spotify')


