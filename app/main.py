import os
import sys
from fastapi import FastAPI
from fastapi.logger import logger
from pydantic import BaseSettings
from fastapi.middleware.cors import CORSMiddleware
from app.routers import oauth
from app.config import CONFIGURATION
from app.brains.database import get_engine
from app import router

class Settings(BaseSettings):
    # ... The rest of our FastAPI settings
    BASE_URL = "http://localhost:8000"
    USE_NGROK = os.environ.get("USE_NGROK", "False") == "True"


def init_webhooks(base_url):
    # Update inbound traffic via APIs to use the public-facing ngrok URL
    pass

SHOW_DOCS = CONFIGURATION['FAST_API_SETTINGS']['SHOW_DOCUMENTATIONS_PATH']

if SHOW_DOCS == 'True':
    docs_url = '/documentation'
else:
    docs_url = None


settings = Settings()
app = FastAPI(docs_url=docs_url)


origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['GET','POST'],
    allow_headers=['*']
)

if settings.USE_NGROK:
    # pyngrok should only ever be installed or initialized in a dev environment when this flag is set
    from pyngrok import ngrok,conf

    # Get the dev server port (defaults to 8000 for Uvicorn, can be overridden with `--port`
    # when starting the server
    port = sys.argv[sys.argv.index("--port") + 1] if "--port" in sys.argv else 8000

    # Open a ngrok tunnel to the dev server
    conf.get_default().region = "us"
    public_url = ngrok.connect(port, subdomain="sound-and-location").public_url
    logger.info("ngrok tunnel \"{}\" -> \"http://127.0.0.1:{}\"".format(public_url, port))

    # Update any base URLs or webhooks to use the public ngrok URL
    settings.BASE_URL = public_url
    init_webhooks(public_url)
    tunnels = ngrok.get_tunnels()
    print("\n Tunnels \n",tunnels, "\n")



@app.on_event("startup")
def open_database_connection_pools():
    get_engine()

@app.on_event("shutdown")
def close_database_connection_pools():
    _db_conn = get_engine()
    if _db_conn:
        _db_conn.dispose()



app.include_router(router.router, prefix="/api/v1")
app.include_router(oauth.router, prefix="/oauth")


@app.get("/")
async def root():
    return {"message": "Welcome to Sound and Location, Solo! If you find yourself here then welcome to the development journey of a new app.", "Author":"Andy Pineda", "Contact":"pinedandyr@gmail.com"}

