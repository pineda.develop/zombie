from uuid import uuid4
from flask_sqlalchemy import SQLAlchemy 
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Text, DateTime, LargeBinary, Float
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import false, null, text, true
from sqlalchemy.dialects.postgresql import UUID
import datetime
from .database import Base
import random, string
from geoalchemy2 import Geography


metadata = Base.metadata
db = SQLAlchemy(session_options={'autocommit': False})
def generate_hash():
    return ''.join(random.choices(string.ascii_letters + string.digits, k=12))


class BaseModel(Base):
    """Base data model for all objects"""
    __abstract__ = True

    def __init__(self, attribute_dict):
        for key, value in attribute_dict.items():
            self.__setattr__(key, value)

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)

    def json(self):
        """
                Define a base way to jsonify models, dealing with datetime objects
        """
        return {
            column: value if not isinstance(value, datetime.date) else value.strftime('%Y-%m-%d %H:%M:%S')
            for column, value in vars(self).items() if not column.startswith('_')
        }


class Establishments(BaseModel):
    __tablename__ = "establishments"

    id = Column(UUID, primary_key=True, server_default=text("uuid_generate_v4()"))
    establishment_id = Column(UUID, unique=True)
    name = Column(Text, unique=False)
    address = Column(Text, unique=False)
    city = Column(Text, unique=False, nullable = False)
    state = Column(Text, nullable=False)
    country = Column(Text,  nullable=False)
    latitude = Column(Float,  nullable=False)
    longitude = Column(Float,  nullable=False)
    geography = Column(Geography)
    created_by_user_id = Column(UUID)
    playlist_id = Column(Text)
    created_time = Column(DateTime(True), nullable=False, server_default=text("now()"))
    updated_time = Column(DateTime(True), nullable=False, onupdate=datetime.datetime.utcnow, default=datetime.datetime.utcnow)
    

    def __init__(self, establishments_user_dictionary):
        self.name = establishments_user_dictionary.get('username')
        self.address = establishments_user_dictionary.get('address')
        self.city = establishments_user_dictionary.get('city')
        self.state = establishments_user_dictionary.get('state')
        self.country = establishments_user_dictionary.get('country')




class SoloUser(BaseModel):
    __tablename__ = 'solo_user'

    id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v4()"))
    user_id = Column(Text, unique=True,server_default=text("uuid_generate_v4()"))
    created_time = Column(DateTime(True), nullable=False, server_default=text("now()"))
    modified_time = Column(DateTime(True), nullable=False, onupdate=datetime.datetime.utcnow, server_default=text("now()"))



class SoloUserLoginCredentials(BaseModel):
    __tablename__ = "solo_user_login_credentials"

    id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v4()"))
    owner_id = Column(UUID, unique=True)
    email = Column(Text, unique=True)
    password = Column(Text, nullable=False)
    salt_hash = Column(Text, nullable=False)
    created_time = Column(DateTime(True), nullable=False, server_default=text("now()"))
    modified_time = Column(DateTime(True), nullable=False, onupdate=datetime.datetime.utcnow, server_default=text("now()"))


class SpotifyAccounts(BaseModel):
    __tablename__  = "spotify_accounts"

    id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v4()"))
    name = Column(Text, nullable=False)
    created_by_user_id = Column(UUID, nullable=False)
    credentials = Column(LargeBinary, nullable=False)
    created_time = Column(DateTime(True), nullable=False, server_default=text("now()"))
    modified_time = Column(DateTime(True), nullable=False, onupdate=datetime.datetime.utcnow, server_default=text("now()"))


class EstablishmentPlaylists(BaseModel):
    __tablename__ = "establishment_playlists"

    
    id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v4()"))
    establishment_id = Column(UUID, unique=True)
    created_by_user_id = Column(UUID, nullable=False)
    playlist_id = Column(Text)
    created_time = Column(DateTime(True), nullable=False, server_default=text("now()"))
    modified_time = Column(DateTime(True), nullable=False, onupdate=datetime.datetime.utcnow, server_default=text("now()"))



class SpotifyClientCredentials(BaseModel):
    __tablename__ = "spotify_client_credentials"
    
    id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v4()"))
    access_token = Column(Text)
    created_time = Column(DateTime(True), nullable=False, server_default=text("now()"))
    modified_time = Column(DateTime(True), nullable=False, onupdate=datetime.datetime.utcnow, server_default=text("now()"))
