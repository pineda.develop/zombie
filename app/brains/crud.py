from sqlalchemy.orm import Session
from . import models, schemas
import random 

# user: schemas.EstablishmentCreate

def create_establishment(db: Session):
    establishment_dictionary = {
        "name":"Sound and Location",
        "address":f"{random.randrange(0,10000)} Harbor Blvd",
        "city":"Weehawken",
        "state":"New Jersey",
        "country":"United States"
    }
    db_establishment = models.Establishments(establishments_user_dictionary=establishment_dictionary)
    db.add(db_establishment)
    db.commit()
    db.refresh(db_establishment)
    return db_establishment



def read_establishments(db: Session):
    db_establishment = models.Establishments
    return db.query(db_establishment).all()