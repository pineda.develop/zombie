from logging.config import fileConfig
from sqlalchemy import engine_from_config
from sqlalchemy import pool
from alembic import context
from app.brains import models
from app.config import CONFIGURATION

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Declare environment variables for database 
section = config.config_ini_section

brains_db_config = CONFIGURATION['DATABASE']

config.set_section_option(section, "BRAINS_DB_USER", brains_db_config.get("USER"))
config.set_section_option(section, "BRAINS_DB_PASSWORD", brains_db_config.get("PASSWORD"))
config.set_section_option(section, "BRAINS_DB_HOST", str(brains_db_config.get("URL")))
config.set_section_option(section, "BRAINS_DB_PORT", str(brains_db_config.get("PORT")))
config.set_section_option(section, "BRAINS_DB_NAME", brains_db_config.get("NAME"))


# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = models.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
        compare_type=True
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata, compare_type = True
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
