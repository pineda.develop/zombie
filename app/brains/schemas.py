from typing import List, Optional
from pydantic import BaseModel


class EstablishmentBase(BaseModel):
    name: str 
    address: str 
    city: str 
    state: str 
    country: str
    
class EstablishmentCreate(EstablishmentBase):
    pass 

class Establishment(EstablishmentBase):
    id:str 
    class Config:
        orm_mode = True


class SoloUserBase(BaseModel):
    name: str 
    email: str 
    password: str 


class SoloUserLoginCredentialsBase(BaseModel):
    email: str 
    password: str 


class FavoriteSong(BaseModel):
    track_id: str
    update_type: str


class Track(BaseModel):
    track_id:str
   