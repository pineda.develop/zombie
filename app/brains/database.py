from sqlalchemy import create_engine, engine 
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session, Session
from app.config import CONFIGURATION
from fastapi import Depends
from contextlib import contextmanager
from typing import Iterable
from functools import lru_cache


SQLALCHEMY_DATABASE_URL = f"postgresql://{CONFIGURATION['DATABASE']['USER']}:{CONFIGURATION['DATABASE']['PASSWORD']}@{CONFIGURATION['DATABASE']['URL']}:{CONFIGURATION['DATABASE']['PORT']}/{CONFIGURATION['DATABASE']['NAME']}"
engine = create_engine(SQLALCHEMY_DATABASE_URL)

@lru_cache()
def get_engine() -> engine:
    return create_engine(
        SQLALCHEMY_DATABASE_URL,
        # connect_args={"check_same_thread": False},
        pool_pre_ping=True,
    )

@contextmanager
def get_db(db_conn=Depends(get_engine)) -> Iterable[Session]:
    session: Session = sessionmaker(autocommit=False, autoflush=False, bind=db_conn, expire_on_commit=False)()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

def get_sessionmaker_instance(uri: str) -> sessionmaker:
    engine = create_engine(uri, pool_pre_ping=True)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    return Session


SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


class SessionManager:
    def __init__(self):
        self.db = SessionLocal()

    def __enter__(self):
        return self.db

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.db.close()

