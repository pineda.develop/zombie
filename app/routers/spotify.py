from fastapi import APIRouter
from fastapi.param_functions import Depends
from fastapi_jwt_auth.auth_jwt import AuthJWT
from app.brains.database import get_db
from app.brains.schemas import FavoriteSong, Track
from app.helpers import spotify_helpers,user_helpers, oauth_helpers
from app.helpers.jwt_helpers import app_access_required
from sqlalchemy.orm.session import Session

router = APIRouter()

@router.get('/')
def welcome_to_spotify_api():
    return {"Message":"Made it to Spotify api!!!"}


@router.get('/welcome')
@app_access_required
def welcome_to_spotify_api():
    return {"Message":"Made it to Spotify api!!!"}

@router.get('/get-spotify-url')
@app_access_required
def spotify_get_url(context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    """Generate a Spotify Auth URL to authorize Sound and Location to users Spotify Account

    Args:
        Authorize (AuthJWT, optional): JWT token for access to API. Defaults to Depends().

    Returns:
        Response (RedirectResponse): a reroute to the authorization page 
    """
    print("IN HERE")
    user_id = Authorize.get_jwt_subject()
    print("User ID", user_id)
    with context_session as db:
        user = user_helpers.get_user_by_id(db, user_id=user_id)
        print("USER", user.__dict__)
        url = oauth_helpers.generate_spotify_url(user_email=user.email,Authorize=Authorize)
        return {"url":url}


@router.get('/check')
@app_access_required
def check_for_account(context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    """GET endpoint for checking if a user has a Spotify account

    Args:
        context_session (Session, optional): a database session. Defaults to Depends(get_db).
        Authorize (AuthJWT, optional): JWT token for access to API. Defaults to Depends().

    Returns:
        bool: True if they have an account, False if they do not
    """
    user_id = Authorize.get_jwt_subject()
    with context_session as db:
        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            return {"has_account":"true"} 

    return {"has_account": "false"} 


@router.post('/delete-account')
@app_access_required
def remove_spotify_account(context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    """Remove a users Spotify account from our database 

    Args:
        context_session (Session, optional): a database connection. Defaults to Depends(get_db).
        Authorize (AuthJWT, optional): Defaults to Depends().

    Returns:
        dict: key = deletd and value is true if account was successfully remove or false if it was not 
    """
    user_id = Authorize.get_jwt_subject()
    with context_session as db:
        deleted_spotify_account = spotify_helpers.delete_spotify_account(db,user_id)
        if deleted_spotify_account:
            return {"deleted": 'true'}
    
    return {'deleted': 'false'}


@router.post('/play')
@app_access_required
def play_on_device( track:Track , context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    """Play a song on users Spotify account to their device 

    Args:
        context_session (Session, optional): a database connection. Defaults to Depends(get_db).
        Authorize (AuthJWT, optional): Defaults to Depends().

    Returns:
        dict: {"status":song_status} , song_status = "PLAYING_SONG", "ERROR"
    """
    user_id = Authorize.get_jwt_subject()
    
    with context_session as db:
        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            song_status = spotify_helpers.play_song(db, user_id, track.track_id)
            return {"status":song_status}

    return {"status": "NO ACCOUNT FOUND"}




@router.post('/pause')
@app_access_required
def pause_on_device( context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):

    user_id = Authorize.get_jwt_subject()
    with context_session as db:

        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            track_status = spotify_helpers.pause_player(db, user_id)
            print('song status', track_status)
            return {"status": track_status}

    return {"status": "NO ACCOUNT FOUND"}




@router.post('/favorite-track')
@app_access_required
def favorite_track_to_saved_songs(track_info: FavoriteSong, context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):

    user_id = Authorize.get_jwt_subject()
    with context_session as db:

        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            track_status = spotify_helpers.update_track_favorite_status(db, user_id, track_info.update_type, track_info.track_id)
            print('track status', track_status)
            return {"status": track_status}

    return {"status": "NO ACCOUNT FOUND"}


@router.post('/unfavorite-track')
@app_access_required
def unfavorite_track_from_saved_songs(track_info: FavoriteSong, context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    print("ASASDASD",track_info)
    user_id = Authorize.get_jwt_subject()
    with context_session as db:

        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            track_status = spotify_helpers.update_track_favorite_status(db, user_id, track_info.update_type, track_info.track_id)
            print('track status', track_status)
            return {"status": track_status}

    return {"status": "NO ACCOUNT FOUND"}



@router.get('/get-all-playlists')
@app_access_required
def get_user_playlists(context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    
    user_id = Authorize.get_jwt_subject()
    with context_session as db:

        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            track_status = spotify_helpers.get_all_playlists(db, user_id)
            print('track status', track_status)
            return {"status": track_status}

    return {"status": "NO ACCOUNT FOUND"}

@router.get('/get-a-playlist')
@app_access_required
def get_a_playlist(playlist_id:str, context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    with context_session as db:
        try: 
            tracks = spotify_helpers.get_playlist(db)
            print("success")
            return {"tracks":tracks}
        except:
            print("fail")
            return {"status": "An error occured retrieving this playlist"}



@router.get('/profile')
@app_access_required
def get_user_profile(context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    
    user_id = Authorize.get_jwt_subject()
    with context_session as db:

        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            profile = spotify_helpers.get_current_user_profile(db, user_id)
            return {"status": "success", "profile":profile}

    return {"status": "NO ACCOUNT FOUND"}




@router.get('/get-saved-tracks')
@app_access_required
def get_user_saved_tracks(track_ids_list: str, context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):

    user_id = Authorize.get_jwt_subject()
    with context_session as db:
        
        spotify_account = spotify_helpers.check_for_spotify_account(db,user_id)
        if spotify_account:
            saved_tracks = spotify_helpers.get_saved_tracks(db, user_id, track_ids_list)
            print("SAVED TRACKS", saved_tracks)
            return {"status": "success", "tracks_favorite_status": saved_tracks}

    return {"status": "Trouble generating track status for saved tracks"}

