from fastapi import APIRouter
from app.brains.database import get_db
from app.helpers import search_google_places
from fastapi.param_functions import Depends
from sqlalchemy.orm.session import Session
from fastapi_jwt_auth import AuthJWT
from app.helpers.jwt_helpers import app_access_required
from app.helpers.search_helpers import search_by_geo_location, search_by_text_input

router = APIRouter()


@router.get('/')
def welcome_to_google_places_api():
    return {"Message":"Made it to search!!!"}

     
@router.get('/google_places')
def google_places(user_input:str):
    places = search_google_places.GooglePlacesAPIHelpers()
    list_of_nearby_places = places.query_auto_suggestion(user_input=user_input)
    return {"Places": list_of_nearby_places}



@router.get('/text-input')
@app_access_required
def search_establishments(text_input, context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    with context_session as db:
        search_dict = search_by_text_input(db, str(text_input))
        return search_dict


@router.get('/nearby')
@app_access_required
def search_establishments_nearby(longitude:float, latitude: float, context_session: Session = Depends(get_db), Authorize:AuthJWT = Depends()):
    with context_session as db:
        search_dict = search_by_geo_location(db, long=longitude, lat=latitude)
        if search_dict:
            return search_dict

