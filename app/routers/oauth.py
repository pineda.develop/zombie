
from fastapi import HTTPException, Depends, Request, APIRouter
from requests.api import request
from starlette.responses import RedirectResponse, HTMLResponse
from app.brains.database import get_db
from app.helpers import hashing_helpers, oauth_helpers
from fastapi.param_functions import Depends
from sqlalchemy.orm.session import Session
from app.brains import models, crud, schemas
from fastapi_jwt_auth import AuthJWT
from urllib.parse import urlencode
from app.helpers.jwt_helpers import app_access_required


router = APIRouter()

@router.get('/')
async def root():
    return {"Message":"Welcome to the oauth secton"}

@router.get('/spotify-callback')
async def spotify_callback(code: str, state:str, Authorize:AuthJWT = Depends(),context_session: Session = Depends(get_db)):
    with context_session as db:
        response = oauth_helpers.receive_spotify_oauth_code(db,state,code)

    response = RedirectResponse(url="/oauth/success")
    return response


@router.get('/success')
def success_message():
    """
        Success HTML response page to present when adding an integration is successful 
    """

    html_content = """
        <html>
        <head>
            <link
            href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap"
            rel="stylesheet"
            />
        </head>
        <style>
            body {
            text-align: center;
            padding: 40px 0;
            background: #ebf0f5;
            }
            h1 {
            color: #88b04b;
            font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
            font-weight: 900;
            font-size: 40px;
            margin-bottom: 10px;
            }
            p {
            color: #404f5e;
            font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
            font-size: 20px;
            margin: 0;
            }
            i {
            color: #9abc66;
            font-size: 100px;
            line-height: 200px;
            margin-left: -15px;
            }
            .card {
            background: white;
            padding: 60px;
            border-radius: 4px;
            box-shadow: 0 2px 3px #c8d0d8;
            display: inline-block;
            margin: 0 auto;
            }
        </style>
        <body>
            <div class="card">
            <div
                style="
                border-radius: 200px;
                height: 200px;
                width: 200px;
                background: #f8faf5;
                margin: 0 auto;
                "
            >
                <i class="checkmark">✓</i>
            </div>
            <h1>Success</h1>
            <p>
                Your Spotify account is now connected.<br />
                Exit this window to go back to the app!
            </p>
            </div>
        </body>
        </html>
    """
    return HTMLResponse(content=html_content, status_code=200)





