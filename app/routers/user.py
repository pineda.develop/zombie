
from fastapi import HTTPException, Depends, Request, APIRouter
from app.brains.database import get_db
from app.helpers import hashing_helpers, jwt_helpers, spotify_helpers,user_helpers
from fastapi.param_functions import Depends
from sqlalchemy.orm.session import Session
from app.brains import models, crud, schemas
from fastapi_jwt_auth import AuthJWT
import random
import string 
from fastapi_jwt_auth.exceptions import JWTDecodeError, AuthJWTException
router = APIRouter()

@router.get('/')
async def root():
    return {"Message":"Welcome to User Settings"}


@router.post('/signup')
def create_tokens(Authorize:AuthJWT = Depends(), context_session: Session = Depends(get_db)):
    """POST Generate a random account to grant token access 

    Args:
        Authorize (AuthJWT, optional): JWT token required for API access. Defaults to Depends().
        context_session (Session, optional): a database connection. Defaults to Depends(get_db).
    """

    
    with context_session as db:
        # Check if the user already exist in the database
        # If they do not then create a new user with the user information  
        try:
            solo_user_info = user_helpers.create_user(db)
            # Generate refresh and access tokens 
            tokens = jwt_helpers.generate_tokens(solo_user_info.user_id)
            spotify_dict = {"has_account":"False"}
        except:    
            return {"status_code": 409,"message":"User already exist with that email" } 
       
            
    return {"user":solo_user_info, "tokens":tokens, "spotify":spotify_dict }



@router.post('/signin')
def login_user(user: schemas.SoloUserLoginCredentialsBase, Authorize:AuthJWT = Depends(), context_session: Session = Depends(get_db)):
    """Login in the user by verifying that their credentials match the ones in the database 

    Args:
        user (schemas.SoloUserLoginCredentialsBase): required params to login in a dictionary, {email, password }
        Authorize (AuthJWT, optional): JWT token required for API access.. Defaults to Depends().
        context_session (Session, optional): a database connection. Defaults to Depends(get_db).

    Raises:
        HTTPException: {status_code:401, message: "Email and password is incorrect"}
        HTTPException: {status_code:404, message: "No account found with that email"}

    Returns:
        user_exist: an object with the basic account information of the user 
    """
    with context_session as db:
        # Check if the user exist in the database 
        # If they do not then raise an error
        user_exist = user_helpers.get_user_by_email(db, user.email)
        if user_exist:
            user_login_credentials = user_helpers.get_user_login_credentials(db, user.email)
            input_password_with_salt_value = user_login_credentials.salt_hash + user.password 
            passwords_match = hashing_helpers.verify_hash_password(input_password_with_salt_value, user_login_credentials.password)
            
            # Check to see if the user entered the correct password for the account associated with the email 
            if passwords_match:

                # Generate refresh and access tokens 
                tokens = jwt_helpers.generate_tokens(user_exist.id)
                # Check if the user has a spotify account 
                user_has_spotify_account = spotify_helpers.check_for_spotify_account(db, user_exist.id)
                spotify_dict = {"has_account": str(user_has_spotify_account).lower()}

                return {"user":user_exist, "tokens":tokens, "spotify":spotify_dict }
            else:
                return {"status_code": 401,"message":"Email and password is incorrect" } 
        else:
            return {"status_code": 404,"message":"No account found with that email" } 
        




@router.get('/tokens/refresh')
def refresh(Authorize: AuthJWT = Depends()):
    
    try:
        Authorize.jwt_refresh_token_required()
        user_id = Authorize.get_jwt_subject()
        app_token = jwt_helpers.create_access_token(user_id, Authorize)
        print("NEW TOKENS", app_token)
        print('user', user_id)
        return app_token
    except JWTDecodeError as e:
        if e.message == 'Signature has expired':
            return {"status_code": 401,"message":"REFRESH_TOKEN_INVALID" } 
 




@router.get("/check")
@jwt_helpers.app_access_required
def check_token(Authorize: AuthJWT = Depends()):
    try:
        current_user = Authorize.get_jwt_subject()
        return {'user': current_user}
    except JWTDecodeError as e:
        if e.message == 'Signature has expired':
            raise HTTPException(status_code=401, detail='Signature has expired')
        else:
            raise Exception(e.message)
