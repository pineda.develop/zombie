import os 
from dotenv import load_dotenv


# # Currently get local environmnet variables
# # To Do: See how it can switch based off stage ex. production, local, staging 
# load_dotenv('/Users/andy/Documents/Developer/GitHub/zombie/.env')

CONFIGURATION = {
    'GOOGLE_PLACES':{
        'API_KEY': os.getenv('GOOGLE_PLACES_API_KEY')
    },
    'AWS':{
        'ACCESS_KEY': os.getenv("AWS_ACCESS_KEY"),
        'SECRET_ACCESS_KEY': os.getenv("AWS_SECRET_ACCESS_KEY")
    },
    "KMS":{
        "SOLO_KEY_ID":os.getenv('SOLO_KEY_ID')
    },

    "KMS":{
        "SOLO_KEY_ID":os.getenv('SOLO_KEY_ID')
    },
    'DATABASE':{
        "USER": os.getenv('DATABASE_USER'),
        "PASSWORD": str(os.getenv('DATABASE_PASSWORD')),
        "URL": os.getenv("DATABASE_SERVER_URL"),
        "PORT":  int(os.getenv("DATABASE_PORT")),
        "NAME": os.getenv("DATABASE_TO_ACCESS")
    },
    'FAST_API_SETTINGS':{
        'SHOW_DOCUMENTATIONS_PATH': 'false',
        'DOMAIN': os.getenv('DOMAIN'),
        'ZOMBIE_API_KEY': os.getenv('ZOMBIE_BACKEND_JWT_TOKEN')
    },
    'SPOTIFY':{
        'CLIENT_ID':os.getenv('SPOTIFY_CLIENT_ID'),
        'CLIENT_SECRET': os.getenv('SPOTIFY_CLIENT_SECRET'),
        'SCOPES': [
                'user-read-playback-state',
                'user-modify-playback-state',
                'user-library-modify',
                'user-library-read'
            ],
        'REDIRECT_URL': 'oauth/spotify-callback',
        'OAUTH_URL':'https://accounts.spotify.com/authorize'
    }
}


