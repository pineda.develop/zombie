## Start local development server

In order to run the local development server and make request to the backend will need to have ngrok intalled

Follow the instructions at `https://ngrok.com/download` and start `ngrok` on port `8000`<br>
`./ngrok http 8000`
<br>
<br>
<strong>Tip....</strong><br>
In order or run the `ngrok` command in the terminal make sure to add the exe file to path /usr/local/bin
<br>
<br>
Go to your where you downloaded ngrok in terminal and run this command in terminal
<br>
`mv ngrok /usr/local/bin`
<br>
<br>
Ngrok has been added to startup within unvicorn is called if USE_NGROK is set to true so just run the command bellow to start up your local server <br>
`USE_NGROK=True uvicorn main:app`

<br> 
Install requirements
pip install -r requirements.txt

<br>
Dont forget to reinstall psycopg2 to the production file
